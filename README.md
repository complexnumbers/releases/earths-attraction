# Build files for "Earth's Attraction" album

Index file: https://complexnumbers.gitlab.io/releases/earths-attraction/

## License
### Earth's Attraction (c) by Victor Argonov Project

Earth's Attraction is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
